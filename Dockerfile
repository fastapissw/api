FROM python:3.9


WORKDIR /app


COPY ./api/requirements.txt /app/requirements.txt


RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt


COPY ./api /app


CMD ["uvicorn", "server.app:app", "--host", "0.0.0.0", "--port", "8001"]
