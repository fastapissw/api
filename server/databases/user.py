import motor.motor_asyncio
from bson.objectid import ObjectId
from decouple import config
from ..auth.auth_handler import signJWT

#MONGO_DETAILS = config('MONGO_DETAILS') # ler do arquivo .env a variavel
MONGO_DETAILS = "mongodb://root:pass12345@mongodb:27017"

client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)

database = client.starwars

user_collection = database.get_collection("users_collection")


# helper
def user_helper(user) -> dict:
    return {
        "id": str(user["_id"]),
        "nome": user["nome"],
        "email": user["email"],
        "password": str(user["password"]),
        "token": str(user["token"])
    }

# Buscando todos os usuários do banco
async def retrieve_users():
    users = []
    async for user in user_collection.find():
        users.append(user_helper(user))
    return users


# Adicionando um usuario no banco
async def add_user(user_data: dict) -> dict:
    user_data["token"] = signJWT(user_data["email"])
    user = await user_collection.insert_one(user_data)
    new_user = await user_collection.find_one({"_id": user.inserted_id})
    return user_helper(new_user)


# Buscando um planeta
async def retrieve_user(user: str) -> dict:
    users = await user_collection.find_one({"email": user.email, "password": user.password})
    if users: 
        return user_helper(users)
