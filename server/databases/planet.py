import motor.motor_asyncio
from bson.objectid import ObjectId
from decouple import config

#MONGO_DETAILS = config('MONGO_DETAILS') # ler do arquivo .env a variavel
MONGO_DETAILS = "mongodb://root:pass12345@mongodb:27017"

client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)

database = client.starwars

planet_collection = database.get_collection("planets_collection")

# helper
def planet_helper(planet) -> dict:
    return {
        "id": str(planet["_id"]),
        "name": planet["name"],
        "climate": planet["climate"],
        "diameter": str(planet["diameter"]),
        "films": str(planet["films"]),
        "population": str(planet["population"]),
    }

# Buscando todos os planetas do banco
async def retrieve_planets():
    planets = []
    async for planet in planet_collection.find():
        planets.append(planet_helper(planet))
    return planets


# Adicionando um planeta no banco
async def add_planet(planet_data: dict) -> dict:
    planet = await planet_collection.insert_one(planet_data)
    new_planet = await planet_collection.find_one({"_id": planet.inserted_id})
    return planet_helper(new_planet)


# Buscando um planeta
async def retrieve_planet(id: str) -> dict:
    filmsx = []
    planet = await planet_collection.find_one({"_id": ObjectId(id)})
    if planet:             
        async for film in planet_collection.find({"_id": { '$in' : planet["films"]['data']}}).toArray():
            filmsx.append(planet_helper(film))

        planet['films'] = filmsx
        return planet_helper(planet)


# Atualizando o planeta
async def update_planet(id: str, data: dict):
    if len(data) < 1:
        return False
    planet = await planet_collection.find_one({"_id": ObjectId(id)})
    if planet:
        updated_planet = await planet_collection.update_one(
            {"_id": ObjectId(id)}, {"$set": data}
        )
        if updated_planet:
            return True
        return False


# Excluindo o planeta do banco
async def delete_planet(id: str):
    planet = await planet_collection.find_one({"_id": ObjectId(id)})
    if planet:
        await planet_collection.delete_one({"_id": ObjectId(id)})
        return True
