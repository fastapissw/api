import motor.motor_asyncio
from bson.objectid import ObjectId
from decouple import config

#MONGO_DETAILS = config('MONGO_DETAILS') # ler do arquivo .env a variavel
MONGO_DETAILS = "mongodb://root:pass12345@mongodb:27017"

client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)

database = client.starwars

film_collection = database.get_collection("films_collection")


# helper
def film_helper(film) -> dict:
    return {
        "id": str(film["_id"]),
        "name": film["name"],
        "release_date": film["release_date"],
    }

# Buscando todos os filmes
async def retrieve_films():
    films = []
    async for film in film_collection.find():
        films.append(film_helper(film))
    return films


# Adicionando um filme
async def add_film(film_data: dict) -> dict:
    film = await film_collection.insert_one(film_data)
    new_film = await film_collection.find_one({"_id": film.inserted_id})
    return film_helper(new_film)


# Buscando um filme
async def retrieve_film(id: str) -> dict:
    film = await film_collection.find_one({"_id": ObjectId(id)})
    if film:
        return film_helper(film)


# Atualizando um filme
async def update_film(id: str, data: dict):
    if len(data) < 1:
        return False
    film = await film_collection.find_one({"_id": ObjectId(id)})
    if film:
        updated_film = await film_collection.update_one(
            {"_id": ObjectId(id)}, {"$set": data}
        )
        if updated_film:
            return True
        return False


# Excluindo um filme
async def delete_film(id: str):
    film = await film_collection.find_one({"_id": ObjectId(id)})
    if film:
        await film_collection.delete_one({"_id": ObjectId(id)})
        return True