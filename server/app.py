from fastapi import FastAPI

from .routes.planet import router as PlanetRouter
from .routes.films import router as FilmRouter
from .routes.user import router as UserRouter

app = FastAPI()

app.include_router(PlanetRouter, tags=["planet"], prefix="/planet")
app.include_router(FilmRouter, tags=["film"], prefix="/film")
app.include_router(UserRouter, tags=["user"], prefix="/user")

@app.get("/", tags=["Root"])
async def read_root():
    return {"message": "Bem-vindo a api SW!"}