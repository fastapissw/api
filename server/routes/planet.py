from fastapi import APIRouter, Body, Depends
from fastapi.encoders import jsonable_encoder
from ..auth.auth_bearer import JWTBearer

from ..databases.planet import (
    add_planet,
    delete_planet,
    retrieve_planets,
    retrieve_planet,
    update_planet,
)
from ..models.planet import (
    ErrorResponseModel,
    ResponseModel,
    PlanetSchema,
    UpdatePlanetModel,
)

router = APIRouter()


@router.post("/", dependencies=[Depends(JWTBearer())], response_description="Objeto Planeta criado!")
async def add_planet_data(planet: PlanetSchema = Body(...)):
    planet = jsonable_encoder(planet)
    new_planet = await add_planet(planet)
    return ResponseModel(new_planet, "Planeta adicionado com sucesso.")


@router.get("/", dependencies=[Depends(JWTBearer())], response_description="Planetas recuperados")
async def get_planets():
    planets = await retrieve_planets()
    if planets:
        return ResponseModel(planets, "Planetas recuperados com sucesso.")
    return ResponseModel(planets, "Lista vazia retornado.")


@router.get("/{id}", dependencies=[Depends(JWTBearer())], response_description="Objeto de planetas recuperados.")
async def get_planet_data(id):
    planet = await retrieve_planet(id)
    if planet:
        return ResponseModel(planet, "Planeta recuparado com sucesso.")
    return ErrorResponseModel("Ocorreu um error.", 404, "Planeta não existe.")


@router.put("/{id}", dependencies=[Depends(JWTBearer())])
async def update_planet_data(id: str, req: UpdatePlanetModel = Body(...)):
    req = {k: v for k, v in req.dict().items() if v is not None}
    updated_planet = await update_planet(id, req)
    if updated_planet:
        return ResponseModel(
            "Planeta com ID: {} nome atualizado com sucesso ".format(id),
            "Nome do planeta atualizado com sucesso",
        )
    return ErrorResponseModel("Ocorreu um error", 404, "Ocorreu um error na atualização dos dados.",
    )


@router.delete("/{id}", dependencies=[Depends(JWTBearer())], response_description="Dados do Planeta foram deletados do banco.")
async def delete_planet_data(id: str):
    deleted_planet = await delete_planet(id)
    if deleted_planet:
        return ResponseModel("Planeta com ID: {} foi removido ".format(id), "Planeta deletado com sucesso.")
    return ErrorResponseModel("Ocorreu um error", 404, "Planeta com ID {0} não existe ".format(id))