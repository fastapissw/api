from fastapi import APIRouter, Body, Depends
from fastapi.encoders import jsonable_encoder

from ..models.user import UserSchema, UserLoginSchema
from ..auth.auth_handler import signJWT

users = []


from ..databases.user import (
    add_user,
    #delete_planet,
    #retrieve_planets,
    retrieve_user,
    #update_planet,
)
from ..models.user import (
    ErrorResponseModel,
    ResponseModel,
    UserSchema,
    UserLoginSchema,
)

router = APIRouter()

# helpers
def check_user(data: UserLoginSchema):
    for user in users:
        if user.email == data.email and user.password == data.password:
            return True
    return False


# route
@router.post("/signup", tags=["user"])
async def add_user_data(user: UserSchema = Body(...)):
    users = await retrieve_user(user)
    if users: 
        return ErrorResponseModel("Ocorreu um error.", 404, "Usuário já existe.")
    user = jsonable_encoder(user)
    new_user = await add_user(user)
    return ResponseModel(new_user, "Usuário adicionado com sucesso.")

@router.post("/login", tags=["user"])
async def user_login(user: UserLoginSchema = Body(...)):
    users = await retrieve_user(user)
    if users:
        return ResponseModel(users, "usuário recuparado com sucesso.")
    return ErrorResponseModel("Ocorreu um error.", 404, "Usuário não existe.")
