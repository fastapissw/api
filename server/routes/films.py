from fastapi import APIRouter, Body, Depends
from fastapi.encoders import jsonable_encoder
from ..auth.auth_bearer import JWTBearer

from ..databases.film import (
    add_film,
    delete_film,
    retrieve_films,
    retrieve_film,
    update_film,
)
from ..models.films import (
    ErrorResponseModel,
    ResponseModel,
    FilmSchema,
    UpdateFilmModel,
)

router = APIRouter()


@router.post("/", dependencies=[Depends(JWTBearer())], response_description="Filme criado!")
async def add_film_data(film: FilmSchema = Body(...)):
    film = jsonable_encoder(film)
    new_film = await add_film(film)
    return ResponseModel(new_film, "Filme adicionado com sucesso.")


@router.get("/", dependencies=[Depends(JWTBearer())], response_description="Filmes recuperados")
async def get_films():
    films = await retrieve_films()
    if films:
        return ResponseModel(films, "Filmes recuperados com sucesso.")
    return ResponseModel(films, "Lista vazia retornado.")


@router.get("/{id}", dependencies=[Depends(JWTBearer())], response_description="Filmes recuperados.")
async def get_film_data(id):
    film = await retrieve_film(id)
    if film:
        return ResponseModel(film, "Filme recuparado com sucesso.")
    return ErrorResponseModel("Ocorreu um error.", 404, "Filme não existe.")


@router.put("/{id}", dependencies=[Depends(JWTBearer())])
async def update_film_data(id: str, req: UpdateFilmModel = Body(...)):
    req = {k: v for k, v in req.dict().items() if v is not None}
    updated_film = await update_film(id, req)
    if updated_film:
        return ResponseModel(
            "Filme com ID: {} atualizado com sucesso ".format(id),
            "Filme atualizado com sucesso",
        )
    return ErrorResponseModel("Ocorreu um error", 404, "Ocorreu um error na atualização dos dados.",
    )


@router.delete("/{id}", dependencies=[Depends(JWTBearer())], response_description="Filme deletado do banco.")
async def delete_film_data(id: str):
    deleted_film = await delete_film(id)
    if deleted_film:
        return ResponseModel("Filme com ID: {} foi removido ".format(id), "Filme deletado com sucesso.")
    return ErrorResponseModel("Ocorreu um error", 404, "Filme com ID {0} não existe ".format(id))