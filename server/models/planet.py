from datetime import datetime, timedelta
from typing import Optional

from pydantic import BaseModel, EmailStr, Field


class PlanetSchema(BaseModel):
    name: str = Field(...)
    climate: str = Field(...)
    diameter: int = Field(...)
    population: int = Field(...)
    films: list = Field(...)
    created: datetime

    class Config:
        schema_extra = {
            "example": {
                "name": "Yavin IV",
                "climate": "temperate, tropical",
                "diameter": "10200",
                "population": "1000",
                "films": [
                    "61966ca96685253c8d915168",
                    "61966cda6685253c8d915169"
                ],
                "created": "2014-12-10T11:37:19.144000Z"
            }
        }


class UpdatePlanetModel(BaseModel):
    name: Optional[str]
    climate: Optional[str]
    diameter: Optional[int]
    population: Optional[int]
    films: Optional[list]
    edited: Optional[datetime]

    class Config:
        schema_extra = {
            "example": {
                "name": "Yavin IV",
                "climate": "temperate, tropical",
                "diameter": "10200",
                "population": "1000",
                "films": [
                    "61966ca96685253c8d915168",
                    "61966cda6685253c8d915169"
                ],
                "edited": "2014-12-20T20:58:18.421000Z"
            }
        }


def ResponseModel(data, message):
    return {
        "data": [data],
        "code": 200,
        "message": message,
    }


def ErrorResponseModel(error, code, message):
    return {"error": error, "code": code, "message": message}