from pydantic import BaseModel, Field, EmailStr


class UserSchema(BaseModel):
    nome: str = Field(...)
    email: EmailStr = Field(...)
    password: str = Field(...)
    token: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "nome": "Sandro Nascimento",
                "email": "sandro@x.com",
                "password": "123qwe.",
                "token": ""
            }
        }

class UserLoginSchema(BaseModel):
    email: EmailStr = Field(...)
    password: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "email": "sandro@x.com",
                "password": "123qwe."
            }
        }


def ResponseModel(data, message):
    return {
        "data": [data],
        "code": 200,
        "message": message,
    }


def ErrorResponseModel(error, code, message):
    return {"error": error, "code": code, "message": message}