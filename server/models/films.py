from datetime import datetime, timedelta
from typing import Optional

from pydantic import BaseModel, EmailStr, Field


class FilmSchema(BaseModel):
    name: str = Field(...)
    release_date: str = Field(...)
    created: datetime

    class Config:
        schema_extra = {
            "example": {
                "name": "A New Hope",
                "release_date": "1977-05-25",
                "created": "2021-11-10T11:37:19.144000Z"
            }
        }


class UpdateFilmModel(BaseModel):
    name: Optional[str]
    release_date: Optional[str]
    edited: Optional[datetime]

    class Config:
        schema_extra = {
            "example": {
                "name": "A New Hope",
                "release_date": "1977-05-25",
                "edited": "2014-12-20T20:58:18.421000Z"
            }
        }


def ResponseModel(data, message):
    return {
        "data": [data],
        "code": 200,
        "message": message,
    }


def ErrorResponseModel(error, code, message):
    return {"error": error, "code": code, "message": message}