from fastapi.testclient import TestClient
from ...server.app import app

# Declaring test client
routes = TestClient(app)

def test_planet():
    response = routes.get("/planet")
    assert response.status_code == 200