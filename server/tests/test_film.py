from fastapi.testclient import TestClient
from ...server.app import app

# Declaring test client
routes = TestClient(app)

def test_film():
    response = routes.get("/film")
    assert response.status_code == 200