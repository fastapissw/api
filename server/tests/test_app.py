from fastapi.testclient import TestClient
from ...server.app import app

# Declaring test client
routes = TestClient(app)

def test_app():
    response = routes.get("/")
    assert response.status_code == 200
    assert response.json() == {"message": "Bem-vindo a api SW!"}